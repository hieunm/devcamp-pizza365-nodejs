"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Khai báo các hằng số
const gSMALL_COMBO = "S";
const gMEDIUM_COMBO = "M";
const gLARGE_COMBO = "L";
const gPIZZA_HAWAI = "hawai";
const gPIZZA_HAI_SAN = "seafood";
const gPIZZA_BACON = "bacon";

// Khai báo đối tượng combo pizza size nhỏ
var gComboSmall = {
  duongKinh: "",
  suonNuong: "",
  salad: "",
  nuocNgot: "",
  thanhTien: "",
};

// Khai báo đối tượng combo pizza size vừa
var gComboMedium = {
  duongKinh: "",
  suonNuong: "",
  salad: "",
  nuocNgot: "",
  thanhTien: "",
};

// Khai báo đối tượng combo pizza size lớn
var gComboLarge = {
  duongKinh: "",
  suonNuong: "",
  salad: "",
  nuocNgot: "",
  thanhTien: "",
};

// Khai báo đối tượng để kiểm tra đơn
var gOder = {
  menuDuocChon: null,
  loaiPizza: "",
  loaiNuocUong: "",
  hoVaTen: "",
  email: "",
  dienThoai: "",
  diaChi: "",
  loiNhan: "",
  voucher: "",
  phanTramGiamGia: 0,
  priceAnnualVND: function () {
    return this.menuDuocChon.priceVND * (1 - this.phanTramGiamGia / 100);
  },
};
// Khai báo đối tượng để gửi đơn
var gObjectRequest = {
  kichCo: "",
  duongKinh: "",
  suon: "",
  salad: "",
  loaiPizza: "",
  idVourcher: "",
  idLoaiNuocUong: "",
  soLuongNuoc: "",
  hoTen: "",
  thanhTien: "",
  email: "",
  soDienThoai: "",
  diaChi: "",
  loiNhan: "",
};

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

// Hàm xử lý sự kiện load trang
function onPageLoading() {
  callAPIGetComboPizzaList();
  callAPITypesPizzaList();
  callAPIGetDrinkList();
}

// Hàm xử lý sự kiện ấn nút chọn combo small
function onBtnSmallSizeClick() {
  //gọi hàm và gán dữ liệu combo small
  var vSelectedCombo = getComboObject(
    gSMALL_COMBO,
    gComboSmall.duongKinh,
    gComboSmall.suonNuong,
    gComboSmall.salad,
    gComboSmall.nuocNgot,
    gComboSmall.thanhTien
  );
  vSelectedCombo.displayInConsoleLog(); //gọi phương thức để in combo ra console
  //gọi hàm để đổi màu và thay đổi selected button
  changeComboButtonColor(gSMALL_COMBO);
}
// Hàm xử lý sự kiện ấn nút chọn combo medium
function onBtnMediumSizeClick() {
  //gọi hàm và gán dữ liệu combo medium
  var vSelectedCombo = getComboObject(
    gMEDIUM_COMBO,
    gComboMedium.duongKinh,
    gComboMedium.suonNuong,
    gComboMedium.salad,
    gComboMedium.nuocNgot,
    gComboMedium.thanhTien
  );
  vSelectedCombo.displayInConsoleLog(); //gọi phương thức để in combo ra console
  //gọi hàm để đổi màu và thay đổi selected button
  changeComboButtonColor(gMEDIUM_COMBO);
}
// Hàm xử lý sự kiện ấn nút chọn combo large
function onBtnLargeSizeClick() {
  //gọi hàm và gán dữ liệu combo large
  var vSelectedCombo = getComboObject(
    gLARGE_COMBO,
    gComboLarge.duongKinh,
    gComboLarge.suonNuong,
    gComboLarge.salad,
    gComboLarge.nuocNgot,
    gComboLarge.thanhTien
  );
  vSelectedCombo.displayInConsoleLog(); //gọi phương thức để in combo ra console
  //gọi hàm để đổi màu và thay đổi selected button
  changeComboButtonColor(gLARGE_COMBO);
}
// Hàm xử lý sự kiện ấn nút chọn combo pizza hawai
function onBtnHawaiTypeClick() {
  changeComboButtonColor(gPIZZA_HAWAI);
}
// Hàm xử lý sự kiện ấn nút chọn combo pizza hải sản
function onBtnHaiSanTypeClick() {
  changeComboButtonColor(gPIZZA_HAI_SAN);
}
// Hàm xử lý sự kiện ấn nút chọn combo pizza bacon
function onBtnBaconTypeClick() {
  changeComboButtonColor(gPIZZA_BACON);
}

// Hàm xử lý sự kiện ấn nút kiểm tra đơn hàng
function onBtnCheckDataClick() {
  //B1: Thu thập dữ liệu
  collectData();
  //B2: Kiểm tra dữ liệu
  var vValidateData = validateData();
  //B3: Xử lý hiển thị
  if (vValidateData) {
    writeDataToOutput();
  }
}

// Hàm xử lý sự kiện ấn nút gửi đơn hàng
function onBtnSendDataClick() {
  //B0: Hiển thị đơn hàng vào console để dễ kiểm tra
  showDataToConsole();
  //B1: Thu thập dữ liệu
  collectDataCreateOrder();
  //B2: Kiểm tra dữ liệu(Bỏ qua vì đã có nút kiểm tra đơn rồi)
  //B3: Call API
  callAPICreateOrder();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

// Hàm xử lý call API get danh sách combo
function callAPIGetComboPizzaList() {
  const vBASE_URL = "https://647406377de100807b1a4df4.mockapi.io/combos";

  $.ajax({
    url: vBASE_URL,
    type: "GET",
    success: function (res) {
      console.log(res);
      showDataComboToWeb(res);
    },
    error: function (paramError) {
      console.log(paramError);
      alert("Có lỗi xảy ra. Vui lòng tải lại trang");
    },
  });
}

// Hàm xử lý call API get loại pizza
function callAPITypesPizzaList() {
  const vBASE_URL = "https://647406377de100807b1a4df4.mockapi.io/types";

  $.ajax({
    url: vBASE_URL,
    type: "GET",
    success: function (res) {
      console.log(res);
      showDataTypeToWeb(res);
    },
    error: function (paramError) {
      console.log(paramError);
      alert("Có lỗi xảy ra. Vui lòng tải lại trang");
    },
  });
}

// Hàm xử lý call API get danh sách drinks
function callAPIGetDrinkList() {
  var vBASE_URL = "http://localhost:8000/api/devcamp-pizza365/drinks";

  $.ajax({
    url: vBASE_URL,
    type: "GET",
    success: function (res) {
      console.log(res);
      loadDrinkListToSelect(res);
    },
    error: function (paramError) {
      console.log(paramError.text);
      alert("Có lỗi xảy ra. Vui lòng tải lại trang");
    },
  });
}

// Hàm kiểm tra voucher
function callAPIGetVoucherID() {
  //truy vấn ô input voucher và lấy giá trị
  var vVoucher = $.trim($("#input-voucher-id").val());
  if (vVoucher === "") {
    return 0;
  }
  // Gọi API
  const vBASE_URL = "http://localhost:8000/api/vouchers/";
  $.ajax({
    url: vBASE_URL + vVoucher,
    type: "GET",
    success: function (res) {
      return res.phanTramGiamGia;
    },
    error: function (paramError) {
      console.log("Không tìm thấy voucher " + paramError.message);
      return null;
    },
  });
}

// Hàm call API tạo mới order
function callAPICreateOrder() {
  const vBASE_URL = "http://localhost:8000/api/devcamp-pizza365/orders";

  $.ajax({
    url: vBASE_URL,
    type: "post",
    contentType: "application/json;charset=UTF-8",
    data: JSON.stringify(gObjectRequest),
    success: function (response) {
      //B4: Xử lý hiển thị
      showSuccessCreateOrder(response);
      clearOrder();
    },
    error: function (error) {
      console.log(error);
      showErrorCreateOrder();
    },
  });
}

// Hàm xử lý show dữ liệu combo ra web
function showDataComboToWeb(paramResponse) {
  // Gán dữ liệu combo trả về cho object để sau này dễ thao tác
  // Sử dụng vòng lặp for
  for (let bI = 0; bI < paramResponse.length; bI++) {
    if (paramResponse[bI].kichCo == gSMALL_COMBO) {
      //Combo small
      gComboSmall.duongKinh = paramResponse[bI].duongKinh;
      gComboSmall.suonNuong = paramResponse[bI].suonNuong;
      gComboSmall.salad = paramResponse[bI].salad;
      gComboSmall.nuocNgot = paramResponse[bI].nuocNgot;
      gComboSmall.thanhTien = paramResponse[bI].thanhTien;
    } else if (paramResponse[bI].kichCo == gMEDIUM_COMBO) {
      //Combo medium
      gComboMedium.duongKinh = paramResponse[bI].duongKinh;
      gComboMedium.suonNuong = paramResponse[bI].suonNuong;
      gComboMedium.salad = paramResponse[bI].salad;
      gComboMedium.nuocNgot = paramResponse[bI].nuocNgot;
      gComboMedium.thanhTien = paramResponse[bI].thanhTien;
    } else if (paramResponse[bI].kichCo == gLARGE_COMBO) {
      //Combo large
      gComboLarge.duongKinh = paramResponse[bI].duongKinh;
      gComboLarge.suonNuong = paramResponse[bI].suonNuong;
      gComboLarge.salad = paramResponse[bI].salad;
      gComboLarge.nuocNgot = paramResponse[bI].nuocNgot;
      gComboLarge.thanhTien = paramResponse[bI].thanhTien;
    }
  }

  // Sử dụng jquery để truy vấn và xử lý hiển thị
  // Truy vấn vùng hiển thị Combo small
  let vCardComboSmall = $("#combo").find("div:eq(1)").find(".card:first");
  vCardComboSmall.find("b:first").html(gComboSmall.duongKinh);
  vCardComboSmall.find("b:eq(1)").html(gComboSmall.suonNuong);
  vCardComboSmall.find("b:eq(2)").html(gComboSmall.salad);
  vCardComboSmall.find("b:eq(3)").html(gComboSmall.nuocNgot);
  vCardComboSmall.find("b:last").html(gComboSmall.thanhTien);
  // Truy vấn vùng hiển thị Combo medium
  let vCardComboMedium = $("#combo").find("div:eq(1)").find(".card:eq(1)");
  vCardComboMedium.find("b:first").html(gComboMedium.duongKinh);
  vCardComboMedium.find("b:eq(1)").html(gComboMedium.suonNuong);
  vCardComboMedium.find("b:eq(2)").html(gComboMedium.salad);
  vCardComboMedium.find("b:eq(3)").html(gComboMedium.nuocNgot);
  vCardComboMedium.find("b:last").html(gComboMedium.thanhTien);
  // Truy vấn vùng hiển thị Combo large
  let vCardComboLarge = $("#combo").find("div:eq(1)").find(".card:last");
  vCardComboLarge.find("b:first").html(gComboLarge.duongKinh);
  vCardComboLarge.find("b:eq(1)").html(gComboLarge.suonNuong);
  vCardComboLarge.find("b:eq(2)").html(gComboLarge.salad);
  vCardComboLarge.find("b:eq(3)").html(gComboLarge.nuocNgot);
  vCardComboLarge.find("b:last").html(gComboLarge.thanhTien);
}

// Hàm xử lý show dữ liệu loại pizza ra web
function showDataTypeToWeb(paramResponse) {
  //Không cần gán cho đối tượng vì không cần sử dụng đến ngoài việc hiển thị
  //Sử dụng vòng lặp for để xử lý hiển thị
  for (let bI = 0; bI < paramResponse.length; bI++) {
    if (paramResponse[bI].loaiPizza == gPIZZA_HAWAI) {
      //truy vấn vùng hiển thị pizza hawai
      let vPizzaHawai = $("#type").find("div:eq(1)").find(".card:first");
      vPizzaHawai
        .find("h3")
        .html(paramResponse[bI].name)
        .css("font-size", "1rem");
      vPizzaHawai.find("p:first").html(paramResponse[bI].note);
      vPizzaHawai.find("p:eq(1)").html(paramResponse[bI].description);
    } else if (paramResponse[bI].loaiPizza == gPIZZA_HAI_SAN) {
      //truy vấn vùng hiển thị pizza hải sản
      let vPizzaHaiSan = $("#type").find("div:eq(1)").find(".card:eq(1)");
      vPizzaHaiSan
        .find("h3")
        .html(paramResponse[bI].name)
        .css("font-size", "1rem");
      vPizzaHaiSan.find("p:first").html(paramResponse[bI].note);
      vPizzaHaiSan.find("p:eq(1)").html(paramResponse[bI].description);
    } else if (paramResponse[bI].loaiPizza == gPIZZA_BACON) {
      //truy vấn vùng hiển thị pizza bacon
      let vPizzaBacon = $("#type").find("div:eq(1)").find(".card:eq(2)");
      vPizzaBacon
        .find("h3")
        .html(paramResponse[bI].name)
        .css("font-size", "1rem");
      vPizzaBacon.find("p:first").html(paramResponse[bI].note);
      vPizzaBacon.find("p:eq(1)").html(paramResponse[bI].description);
    }
  }
}

// Hàm xử lý đổ danh sách drinks vào thẻ select
function loadDrinkListToSelect(paramResponse) {
  for (var bI = 0; bI < paramResponse.length; bI++) {
    $("<option>")
      .val(paramResponse[bI].maNuocUong)
      .text(paramResponse[bI].tenNuocUong)
      .appendTo($("#select-drink"));
  }
}

// Hàm thu thập dữ liệu chọn combo
function getComboObject(
  paramMenuName,
  paramDuongKinh,
  paramSuonNuong,
  paramSalad,
  paramDrink,
  paramPrice
) {
  var vMenu = {
    menuName: paramMenuName,
    duongKinhCM: paramDuongKinh,
    suonNuong: paramSuonNuong,
    saladGr: paramSalad,
    drink: paramDrink,
    priceVND: parseFloat(paramPrice.replace(/\./g, "").replace(",", ".")),
    displayInConsoleLog: function () {
      console.log("Kích cỡ: " + this.menuName);
      console.log("Đường kính: " + this.duongKinhCM + " cm");
      console.log("Sườn nướng: " + this.suonNuong);
      console.log("Salad: " + this.saladGr + " gr");
      console.log("Nước ngọt: " + this.drink);
      console.log("Giá tiền: " + this.priceVND + " VNĐ");
    },
  };
  return vMenu;
}

// Hàm đổi màu nút bấm
function changeComboButtonColor(paramCombo) {
  var vBtnSizeSmall = document.getElementById("btn-size-small");
  var vBtnSizeMedium = document.getElementById("btn-size-medium");
  var vBtnSizeLarge = document.getElementById("btn-size-large");
  var vBtnTypeHawai = document.getElementById("btn-type-hawai");
  var vBtnTypeHaiSan = document.getElementById("btn-type-hai-san");
  var vBtnTypeBacon = document.getElementById("btn-type-bacon");

  if (paramCombo === gSMALL_COMBO) {
    vBtnSizeSmall.className = "btn-yellow";
    vBtnSizeMedium.className = "btn-green";
    vBtnSizeLarge.className = "btn-green";

    vBtnSizeSmall.setAttribute("data-is-selected", "Y");
    vBtnSizeMedium.setAttribute("data-is-selected", "N");
    vBtnSizeLarge.setAttribute("data-is-selected", "N");
  } else if (paramCombo === gMEDIUM_COMBO) {
    vBtnSizeSmall.className = "btn-green";
    vBtnSizeMedium.className = "btn-yellow";
    vBtnSizeLarge.className = "btn-green";

    vBtnSizeSmall.setAttribute("data-is-selected", "N");
    vBtnSizeMedium.setAttribute("data-is-selected", "Y");
    vBtnSizeLarge.setAttribute("data-is-selected", "N");
  } else if (paramCombo === gLARGE_COMBO) {
    vBtnSizeSmall.className = "btn-green";
    vBtnSizeMedium.className = "btn-green";
    vBtnSizeLarge.className = "btn-yellow";

    vBtnSizeSmall.setAttribute("data-is-selected", "N");
    vBtnSizeMedium.setAttribute("data-is-selected", "N");
    vBtnSizeLarge.setAttribute("data-is-selected", "Y");
  } else if (paramCombo === gPIZZA_HAWAI) {
    vBtnTypeHawai.className = "btn-yellow";
    vBtnTypeHaiSan.className = "btn-green";
    vBtnTypeBacon.className = "btn-green";

    vBtnTypeHawai.setAttribute("data-is-selected", "Y");
    vBtnTypeHaiSan.setAttribute("data-is-selected", "N");
    vBtnTypeBacon.setAttribute("data-is-selected", "N");
  } else if (paramCombo === gPIZZA_HAI_SAN) {
    vBtnTypeHawai.className = "btn-green";
    vBtnTypeHaiSan.className = "btn-yellow";
    vBtnTypeBacon.className = "btn-green";

    vBtnTypeHawai.setAttribute("data-is-selected", "N");
    vBtnTypeHaiSan.setAttribute("data-is-selected", "Y");
    vBtnTypeBacon.setAttribute("data-is-selected", "N");
  } else if (paramCombo === gPIZZA_BACON) {
    vBtnTypeHawai.className = "btn-green";
    vBtnTypeHaiSan.className = "btn-green";
    vBtnTypeBacon.className = "btn-yellow";

    vBtnTypeHawai.setAttribute("data-is-selected", "N");
    vBtnTypeHaiSan.setAttribute("data-is-selected", "N");
    vBtnTypeBacon.setAttribute("data-is-selected", "Y");
  }
}

// Hàm thu thập dữ liệu đầu vào
function collectData() {
  // Gán giá trị vào object
  gOder.loaiNuocUong = $("#select-drink").val();
  gOder.hoVaTen = $.trim($("#input-name").val());
  gOder.email = $.trim($("#input-email").val());
  gOder.dienThoai = $.trim($("#input-phone").val());
  gOder.diaChi = $.trim($("#input-address").val());
  gOder.loiNhan = $.trim($("#input-message").val());
  gOder.voucher = $.trim($("#input-voucher-id").val());

  // Truy vấn các button để kiểm tra giá trị data-is-selected
  var vBtnSizeSmallSelected = $("#btn-size-small").attr("data-is-selected");
  var vBtnSizeMediumSelected = $("#btn-size-medium").attr("data-is-selected");
  var vBtnSizeLargeSelected = $("#btn-size-large").attr("data-is-selected");
  var vBtnTypeHawaiSelected = $("#btn-type-hawai").attr("data-is-selected");
  var vBtnTypeHaiSanSelected = $("#btn-type-hai-san").attr("data-is-selected");
  var vBtnTypeBaconSelected = $("#btn-type-bacon").attr("data-is-selected");

  //kiểm tra các giá trị data-is-selected
  if (vBtnSizeSmallSelected === "Y") {
    gOder.menuDuocChon = getComboObject(
      gSMALL_COMBO,
      gComboSmall.duongKinh,
      gComboSmall.suonNuong,
      gComboSmall.salad,
      gComboSmall.nuocNgot,
      gComboSmall.thanhTien
    );
  } else if (vBtnSizeMediumSelected === "Y") {
    gOder.menuDuocChon = getComboObject(
      gMEDIUM_COMBO,
      gComboMedium.duongKinh,
      gComboMedium.suonNuong,
      gComboMedium.salad,
      gComboMedium.nuocNgot,
      gComboMedium.thanhTien
    );
  } else if (vBtnSizeLargeSelected === "Y") {
    gOder.menuDuocChon = getComboObject(
      gLARGE_COMBO,
      gComboLarge.duongKinh,
      gComboLarge.suonNuong,
      gComboLarge.salad,
      gComboLarge.nuocNgot,
      gComboLarge.thanhTien
    );
  }
  if (vBtnTypeHawaiSelected === "Y") {
    gOder.loaiPizza = gPIZZA_HAWAI;
  } else if (vBtnTypeHaiSanSelected === "Y") {
    gOder.loaiPizza = gPIZZA_HAI_SAN;
  } else if (vBtnTypeBaconSelected === "Y") {
    gOder.loaiPizza = gPIZZA_BACON;
  }
  //gọi hàm để lấy và gán giá trị % giảm giá
  gOder.phanTramGiamGia = callAPIGetVoucherID();
}

// Hàm thu thập dữ liệu gửi đơn
function collectDataCreateOrder() {
  //gán dữ liệu từ form cho đối tượng gửi đơn thông qua đối tượng kiểm tra đơn
  gObjectRequest.kichCo = gOder.menuDuocChon.menuName;
  gObjectRequest.duongKinh = gOder.menuDuocChon.duongKinhCM;
  gObjectRequest.suon = gOder.menuDuocChon.suonNuong;
  gObjectRequest.salad = gOder.menuDuocChon.saladGr;
  gObjectRequest.loaiPizza = gOder.loaiPizza;
  gObjectRequest.idVourcher = gOder.voucher;
  gObjectRequest.idLoaiNuocUong = gOder.loaiNuocUong;
  gObjectRequest.soLuongNuoc = gOder.menuDuocChon.drink;
  gObjectRequest.hoTen = gOder.hoVaTen;
  gObjectRequest.thanhTien = gOder.priceAnnualVND();
  gObjectRequest.email = gOder.email;
  gObjectRequest.soDienThoai = gOder.dienThoai;
  gObjectRequest.diaChi = gOder.diaChi;
  gObjectRequest.loiNhan = gOder.loiNhan;
}

// Hàm kiểm tra dữ liệu đầu vào
function validateData() {
  if (gOder.menuDuocChon === null) {
    alert("Phải chọn combo");
    return false;
  }
  if (gOder.loaiPizza === "") {
    alert("Phải chọn loại pizza");
    return false;
  }
  if (gOder.loaiNuocUong == 0) {
    alert("Phải chọn loại đồ uống");
    return false;
  }
  if (
    gOder.hoVaTen === "" ||
    gOder.email === "" ||
    gOder.dienThoai === "" ||
    gOder.diaChi === ""
  ) {
    alert("Vui lòng nhập đầy đủ thông tin tại các trường có dấu (*)");
    return false;
  }
  if (!kiemTraEmail(gOder.email)) {
    alert("Email không hợp lệ");
    return false;
  }
  if (!kiemTraNumberPhone(gOder.dienThoai)) {
    alert("Số điện thoại không hợp lệ");
    return false;
  }
  if (gOder.phanTramGiamGia === null) {
    alert("Mã giảm giá không hợp lệ");
    return false;
  }
  return true;
}
/*hàm kiểm tra email
  Nếu khớp, test() trả về true; nếu không khớp, nó trả về false.
  */
function kiemTraEmail(paramEmail) {
  var vBieuthuc =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return vBieuthuc.test(paramEmail);
}
/*hàm kiểm tra sđt
Nếu khớp, test() trả về true; nếu không khớp, nó trả về false.
*/
function kiemTraNumberPhone(paramNumberPhone) {
  var vBieuthuc = /^(?:\+84|0[0-9]{1,2})[0-9]{8,9}$/;
  return vBieuthuc.test(paramNumberPhone);
}

// Hàm hiển thị dữ liệu
function writeDataToOutput() {
  //gán giá trị cho biến
  var vOutput = "";
  vOutput += "Họ và tên: " + gOder.hoVaTen + "<br/>";
  vOutput += "Email: " + gOder.email + "<br/>";
  vOutput += "Số điện thoại: " + gOder.dienThoai + "<br/>";
  vOutput += "Địa chỉ: " + gOder.diaChi + "<br/>";
  vOutput += "Lời nhắn: " + gOder.loiNhan + "<br/>";
  vOutput += `<hr style="border: none; border-bottom: 2px dotted white; margin: 10px 0; width: 25%;">`;
  vOutput += "Kích cỡ: " + gOder.menuDuocChon.menuName + "<br/>";
  vOutput += "Đường kính: " + gOder.menuDuocChon.duongKinhCM + "<br/>";
  vOutput += "Sườn nướng: " + gOder.menuDuocChon.suonNuong + "<br/>";
  vOutput += "Salad: " + gOder.menuDuocChon.saladGr + "<br/>";
  vOutput += "Nước ngọt: " + gOder.menuDuocChon.drink + "<br/>";
  vOutput += "Loại nước uống: " + gOder.loaiNuocUong + "<br/>";
  vOutput += `<hr style="border: none; border-bottom: 2px dotted white; margin: 10px 0; width: 25%;">`;
  vOutput += "Loại Pizza: " + gOder.loaiPizza + "<br/>";
  vOutput += "Mã voucher: " + gOder.voucher + "<br/>";
  vOutput += "Giá VNĐ: " + gOder.menuDuocChon.priceVND + "<br/>";
  vOutput += "Discount %: " + gOder.phanTramGiamGia + "<br/>";
  vOutput += "Phải thanh toán VNĐ: " + gOder.priceAnnualVND() + "<br/>";

  // Truy vấn và hiển thị nội dung trong ô hiển thị
  $("#div-order-infor").html(vOutput);
  $("#div-container-order").show();

  // Ẩn vùng hiển thị thông tin gửi đơn
  $("#div-create-order").hide();

  // Thêm class và hiển thị vùng thông tin gửi đơn
  $("#div-container-order").addClass("bg-info");
  $("#div-order-infor").show();
}

//hàm hiển thị thông tin đặt hàng vào console
function showDataToConsole() {
  // Truy vấn thẻ div
  var vVungHienThi = document.getElementById("div-order-infor");

  // Lấy danh sách các nút con (nodes) bên trong thẻ div
  var vChildNodes = vVungHienThi.childNodes;

  // Duyệt qua từng nút con và kiểm tra kiểu
  for (var i = 0; i < vChildNodes.length; i++) {
    var vNode = vChildNodes[i];

    // Kiểm tra xem nút con có kiểu là "text" hay không
    // nodeType và nodeValue là 2 thuộc tính của phần tử node
    if (vNode.nodeType === 3) {
      // In ra console nếu là văn bản
      console.log(vNode.nodeValue.trim());
    }
    // Kiểm tra nếu phần tử là thẻ hr thì in ra console
    else if (vNode.nodeName.toLowerCase() === "hr") {
      var bPrint = ".".repeat(50);
      console.log(bPrint);
    }
  }
}

// Hàm xử lý hiển thị sau khi gửi đơn thành công
function showSuccessCreateOrder(paramCreateOrderResponse) {
  //Ở đây dùng phương thức DOM. Nếu dùng phương thức thêm các phần tử HTML trong bài này sẽ ngắn gọn và dễ hiểu hơn
  // Truy vấn và ẩn phần kiểm tra đơn hàng, thêm mới vùng hiển thị thông tin tạo đơn hàng
  $("#div-container-order").removeClass("bg-info");
  $("#div-order-infor").hide();

  // Hiển thị vùng hiển thị thông tin gửi đơn
  let vCreateOrder = $("#div-create-order").removeClass("bg-danger").addClass("bg-success").empty().show();

  // Tạo và thêm nội dung thông tin đơn hàng
  $("<p>").addClass("text-center").html("Cảm ơn bạn đã đặt hàng tại Pizza 365. Mã đơn hàng của bạn là:").appendTo(vCreateOrder);
  $("<div>").addClass("form-row form-group d-flex justify-content-center")
    .append(
      $("<label>").addClass("col-sm-2 col-form-label").html("Mã đơn hàng:"),
      $("<input>").addClass("col-sm-4 form-control").attr({type: "text", value: paramCreateOrderResponse.orderCode, disabled: true})
    ).appendTo(vCreateOrder);
}

// Hàm xử lý hiển thị sau khi gửi đơn thất bại
function showErrorCreateOrder() {
  //Ở đây dùng phương thức DOM. Nếu dùng phương thức thêm các phần tử HTML trong bài này sẽ ngắn gọn và dễ hiểu hơn
  // Truy vấn và xử lý vùng hiển thị thông tin đơn hàng
  $("#div-container-order").removeClass("bg-success").removeClass("bg-info"); // Xóa class bg-info để không bị lẫn màu
  $("#div-order-infor").hide(); // Ẩn vùng hiển thị thông tin đơn hàng

  // Vùng hiển thị thông tin gửi đơn
  let vCreateOrder = $("#div-create-order"); // Truy vấn vùng hiển thị thông tin gửi đơn
  vCreateOrder.addClass("bg-danger").empty().show(); // Thêm class bg-danger, xóa nội dung và hiển thị vùng thông tin gửi đơn

  // Tạo thẻ p chứa message và thêm vào vùng hiển thị thông tin gửi đơn
  $("<p>").addClass("text-center").html("Tạo mới đơn hàng thất bại. Đã có lỗi xảy ra. Bạn vui lòng kiểm tra lại thông tin đơn hàng hoặc nhấn lại nút 'Gửi đơn' sau vài phút nhé").appendTo(vCreateOrder);
}

//Hàm xử lý clear form sau khi đã tạo order success
function clearOrder() {
  //reset obj order
  gOder = {
    menuDuocChon: null,
    loaiPizza: "",
    loaiNuocUong: "",
    hoVaTen: "",
    email: "",
    dienThoai: "",
    diaChi: "",
    loiNhan: "",
    voucher: "",
    phanTramGiamGia: 0,
    priceAnnualVND: function () {
      return this.menuDuocChon.priceVND * (1 - this.phanTramGiamGia / 100);
    },
  };
  //clear form
  $("#select-drink").val("0");
  $("#input-name").val("");
  $("#input-email").val("");
  $("#input-phone").val("");
  $("#input-address").val("");
  $("#input-message").val("");
  $("#input-voucher-id").val("");
  //clear button not button send data
  $(".btn-yellow").not("#btn-send-order").toggleClass("btn-yellow btn-green").attr("data-is-selected", "N");
}