# Project Pizza 365 Restaurant

## 🌟 Code Explanation

> ### 1. Front-End

- **_Trang chủ(Home Page)_**
- Khi tải trang sẽ call API lấy danh sách combo, danh sách các loại pizza và danh sách nước uống để xử lý hiển thị.
- Gán thông tin combo và type pizza vào biến toàn cục.
- Khi bấm các nút chọn combo và type thì sẽ lấy thông tin combo và type ở biến toàn cục lưu vào đối tượng request Object và xử lý đổi màu nút bấm.
- Khi bấm nút kiểm tra đơn thì sẽ thực hiện 3 bước xử lý sự kiện(thu thập, kiểm tra và callAPI). Yêu cầu bắt buộc chọn combo, type, drink và nhập đầy đủ các trường có dấu (*). Riêng voucherId không bắt buộc, nếu nhập thì sẽ call API để check thông tin voucher.
- Sau khi đã thỏa mãn các yêu cầu thì sẽ in thông tin order ra web để khách hàng kiểm tra.
- Khi bấm nút gửi đơn thì sẽ thực hiện 3 bước xử lý sự kiện(thu thập, kiểm tra và callAPI). Đầu tiên in order vào console, thu thập dữ liệu và call API tạo mới order. Sau khi nhận phản hồi thì sẽ ẩn vùng thông tin order và hiển thị thông báo tương ứng trạng thái callAPI.
- Nếu tạo order thành công thì sẽ hiển thị thông báo thành công mà mã order. Sau đó clear thông tin order và reset lại web để tránh khách hàng ấn gửi 2 lần.
- Nếu tạo mới thất bại thì sẽ hiển thị thông báo thất bại.

- **_Trang admin(Admin Page)_**
- _Hiển thị danh sách đơn hàng_
> - Khi tải trang sẽ call API lấy thông tin danh sách order rồi đổ thông tin vào bảng.
> - Khi bấm nút tạo mới thì sẽ chuyển hướng tới trang tạo mới order
> - Khi bấm nút order detail thì sẽ lấy id và ordercode trong dataset của order tương ứng. Sau đó truyền qua querystring và chuyển hướng tới trang order detail.
> - Khi bấm nút order delete thì sẽ lấy id trong dataset của order tương ứng. Sau đó hỏi user xem có muốn xóa hay không. Sau đó mới xử lý theo 3 bước xử lý sự kiện.

- _Tạo mới đơn hàng_
> - Khi click nút create thì sẽ thực hiện 3 bước xử lý sự kiện là thu thập dữ liệu, validate dữ liệu trên form và call API.
> - Sau khi nhận được phản hồi thì sẽ xử lý hiển thị tương ứng.
> - Sử dụng confirm để hỏi ý kiến người dùng có tiếp tục ở lại trang lại trở về trang order list.
> - Khi bấm nút back thì sẽ trở về trang order list.

> - _Edit đơn hàng_
> - Khi tải trang thì sẽ lấy thông tin id và ordercode trong querystring.
> - Tiến hành call API để lấy danh sách drink và thông tin order để đổ form.
> - Khi click nút confirm thì sẽ thực hiện 3 bước xử lý sự kiện là thu thập dữ liệu, validate dữ liệu trên form và call API.
> - Nếu status không phải 'confirm' thì sẽ cảnh báo yêu cầu phải chọn lại.
> - Xử lý tương tự với nút cancel.
> - Khi bấm nút back thì sẽ trở về trang order list.

> ### 2. Back-End
- **_Models_**
- Import thư viện Mongoose
- Khai báo đối tượng Schema
- Khởi tạo một Schema trong Mongoose với các thuộc tính cần thiết.
- Tạo 1 Model đối với Schema vừa khởi tạo và exports.
> ### 2. Controllers
- Import thư viện Mongoose và các Model cần thiết.
- Định nghĩa các hàm bất đồng bộ để xử lý các yêu cầu call API.
- Các queries trong Mongoose là bất đồng bộ nên sẽ cần sử dụng await trước mỗi truy vấn query.
- Đặt toàn bộ hàm trong khối try-catch để bắt lỗi hiệu quả tránh treo server khi xảy ra lỗi bất ngờ.
- Nếu đối tượng có các trường tham chiếu đến Model khác, sử dụng populate() để lấy thông tin đầy đủ(có thể populate() lồng nhau hoặc nhiều Model 1 lúc).
- Mỗi khi có lỗi xảy ra thì sẽ xử lý phản hồi tới client và code sẽ ngừng ngay lập tức.
- Một số status chính dùng để phản hồi tới client là:
  > - 200: Khi response trả về thành công
  > - 201: (Chỉ dành cho POST) Khi tạo dữ liệu thành công
  > - 204: (Chỉ dành cho DELETE) Khi xóa thành công. Ta cũng có thể sử dụng status 200 cho DELETE nếu như muốn phản hồi thông điệp về cho client
  > - 400: Khi input truyền vào từ request (query, params hoặc body JSON) không hợp lệ
  > - 404: Khi không tìm thấy bản ghi trên CSDL
  > - 500: Khi code bị lỗi
- Trong mỗi hàm sẽ có thứ tự xử lý như sau:
  > - _Bước 1: Thu thập dữ liệu_
  > - _Bước 2: Kiểm tra dữ liệu_
  > - _Bước 3: Xử lý dữ liệu_
- Sẽ có các controller cơ bản như sau:
  > - _Get Alls_
  > - _Get By Id_
  > - _Create_
  > - _Update_
  > - _Delete_
  >
> ### 3. Middlewares
- Middleware in ra method, originalUrl và thời gian của request.
> ### 4. Routes
- Import thư viện express
- Import controllers vừa tạo sử dụng phép gán phá hủy cấu trúc đối tượng
- Khởi tạo 1 router bằng express
- Dùng phương thức router.<method>() (trong đó <method> là phương thức HTTP như post, get, put, delete) để định nghĩa các endpoint API và xác định các handlers(controllers) cho chúng
- Gán các hàm xử lý (handlers) tương ứng cho mỗi endpoint API
- Định nghĩa đường dẫn cho mỗi endpoint sử dụng format Restful API
> ### 5. App
- File khởi tạo chính của dự án
- Import các thư viện express, mongoose, path, cors
- Import các router
- Định nghĩa các biến app và port
- Thêm middleware express.json(), express.static(), cors() và custom middleware vừa định nghĩa.
- Trỏ đường dẫn tới tới file home page
- Định nghĩa các router API
- Kết nối với MongoDB
- Khởi tạo dự án chạy trong môi trường NodeJs, dữ liệu server sẽ được lắng nghe tại cổng 8000(port) để tránh nhầm lẫn với các cổng hệ thống.

## 📄 Description

> ### 1. Tổng quan
- Trang bắt đầu của dự án là trang Home Page
- Hiển thị thông tin các combo và kiểu pizza.
- Khách hàng có thể tự lựa chọn đơn hàng và gửi thông tin cá nhân để đặt hàng. Đồng thời có thể kiểm tra xem trạng thái đặt hàng đã thành công hay thất bại.
- Trang Admin Page sẽ có thể theo dõi danh sách đơn hàng, tạo mới, sửa và xóa đơn hàng.
> ### 2. Cách tổ chức thư mục dự án
- Sử dụng mô hình MVC (Model-View-Controller) trong NodeJs để tổ chức thư mục dự án.
- File khởi tạo chính của dự án là _index.js_.
- Thư mục app chứa 4 tổ chức file chính đó là _routes_, _middleware_, _controllers_, _models_.
- Các file trong dự án liên kết với nhau theo chuẩn CommonJS.
> ### 3. Luồng thực thi của dự án
- Dự án sẽ được bắt đầu từ file chính index.js
- Client sẽ sử dụng API để tương tác với server
- API sau khi được gọi sẽ lần lượt chạy qua routes -> middlewares -> controllers -> CSDL.

## ✨ Feature
> ### 1. Trang chủ(Home Page)
- ***Main interface***: _Giao diện chính_
+ User portal ***Giao diện người dùng***
![Home page](./views/images/home_page.png)
+ *Giới thiệu cửa hàng*, Giá trị hướng tới
![Introduce](./views/images/gioi_thieu.png)
+ Lựa chọn menu combo phù hợp
![Menu Combo](./views/images/menu_combo.png)
+ Lựa chọn hương vị pizza yêu thích
![Pizza Type](./views/images/loai_pizza.png)
+ Lựa chọn đồ uống kèm theo
![Drink](./views/images/do_uong.png)
+ *Đặt hàng*, Thông tin liên hệ
![Dathang](./views/images/lien_he_dat_hang.png)
+ *Đặt hàng*, Kiểm tra đơn hàng
![Dathang](./views/images/kiem_tra_don_hang.png)
+ *Đặt hàng*, Xác nhận đơn hàng
![Dathang](./views/images/xac_nhan_dat_hang.png)

> ### 2. Trang admin(Admin Page)
- ***Main interface***: _Giao diện chính_
+ Admin portal ***quản trị danh sách đơn hàng***
![Home page](./views/admin/images/01_img_chuc_nang_hien_ds_don_hang.png)
+ *Đặt hàng*, liên hệ
![Dathang](./views/admin/images/02_img_chuc_nang_tao_don_hang.png)
+ Confirm hoặc cancel order
![Dathang](./views/admin/images/03_img_chuc_nang_sua_don_hang.png)
+ Xóa order
![Dathang](./views/admin/images/04_img_chuc_nang_xoa_don_hang.png)


## 🧱 Technology

> ### Front-end:
> 1. [Bootstrap 4](https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js)
> 2. [Jquery 3](https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js)
> 3. [Font Awesome 4](https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css)
> 4. Ajax
> 5. JSON
> 6. Javascript

> ### Back-end:
> 1. [Node.js](https://nodejs.org/en)
> 2. [Express.js(4.18.3)](https://expressjs.com/)
> 3. [Mongoose.js(8.2.1)](https://mongoosejs.com/)
> 4. [Cors(2.8.5)](https://expressjs.com/en/resources/middleware/cors.html)
> 5. [UUID(9.0.1)](https://www.npmjs.com/package/uuid)
> 6. [Nodemon(3.1.0)](https://www.npmjs.com/package/nodemon)

> ### Database:
> 1. [NoSQL - MongoDB](https://www.mongodb.com/)
