//import thư viện
const express = require("express");//import thư viện express
const path = require("path");//import thư viện path của express
const mongoose = require("mongoose");//import thư viện mongoose
const cors = require('cors');
//import router
//for user
const devcampPizza365Router = require('./app/routes/devcamp-pizza365.router');
const initializeData = require('./app/data/data');
//for admin
const drinkRouter = require("./app/routes/drink.router");
const voucherRouter = require("./app/routes/voucher.router");
const userRouter = require("./app/routes/user.router");
const orderRouter = require("./app/routes/order.router");

const app = express();//tạo app
const port = 8000;//định nghĩa cổng

//Định nghĩa middleware static cho folder views để lấy các tệp tĩnh(CSS, hình ảnh,...)
app.use(express.static("views"));
app.use(express.json());//áp dụng middleware json để đọc đc dữ liệu json trong body
app.use(cors());

//Định nghĩa các route cho trang chủ
app.get('/', (req, res) => {
  //Handle đường dẫn đến file HTML trang chủ trong thư mục views
  //HTML có thể chứa các nội dung động nên cần handle
  //Biến __dirname là một biến toàn cục trong Node.js, được định nghĩa sẵn trong môi trường thực thi Node.js
  //__dirname là đường dẫn tuyệt đối đến thư mục chứa tệp JavaScript hiện đang được thực thi.
  //join sẽ gộp đường dẫn
  const indexPath = path.join(__dirname, './views/pizza365index.html')
  res.sendFile(indexPath);//gửi file tại đường dẫn được chỉ định (được lưu trữ trong indexPath) đến trình duyệt.
});

//định nghĩa router cho trang admin
app.get('/admin', (req, res) => {
  const indexPath = path.join(__dirname, './views/admin/indexListView.html');
  res.sendFile(indexPath);
})

// Kết nối đến cơ sở dữ liệu MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
  .then(() => {
    initializeData();//khởi tạo data
    console.log("Successfully connected to MongoDB");
  })
  .catch((err) => {
    console.log(err.message);
  });

// Áp dụng router
//for user
app.use("/api/devcamp-pizza365", devcampPizza365Router);
//for admin
app.use("/api/drinks", drinkRouter);
app.use("/api/vouchers", voucherRouter);
app.use("/api/users", userRouter);
app.use("/api/orders", orderRouter);

// Khởi tạo máy chủ
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
