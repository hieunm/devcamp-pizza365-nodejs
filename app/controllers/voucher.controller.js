const mongoose = require("mongoose");
const voucherModel = require("./../models/voucher.model");

//get all
const getAllVouchers = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    //B2: Validate dữ liệu
    //B3: Xử lý dữ liệu
    let result = await voucherModel.find();
    res.status(200).json({
      message: "Lay danh sach voucher thanh cong",
      data: result,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//get by id
const getVoucherById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let voucherId = req.params.voucherId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
      return res.status(400).json({
        message: "Id khong hop le",
      });
    }
    //B3: Xử lý dữ liệu
    let result = await voucherModel.findById(voucherId);
    if (!result) {
      return res.status(404).json({
        message: "Khong tim thay voucher",
      });
    }
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//create
const createVoucher = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let { maVoucher, phanTramGiamGia, ghiChu } = req.body;
    //B2: Validate dữ liệu
    if (!maVoucher) {
      return res.status(400).json({
        message: "Ma voucher khong hop le",
      });
    }
    if (!Number.isInteger(phanTramGiamGia) || phanTramGiamGia < 0) {
      //1 số nguyên > 0. Kiểm tra số thực thì dùng Number.isFinite() (isFinite là kiểm tra 1 số hữu hạn)
      //ngoài ra có thể dùng Number.isSafeInteger để kiểm tra số nguyên an toàn(nằm trong khoảng giá trị của type Number)
      return res.status(400).json({
        message: "Phan tram giam gia khong hop le",
      });
    }
    //B3: Xử lý dữ liệu
    let newVoucher = {
      maVoucher,
      phanTramGiamGia,
      ghiChu,
    };
    let result = await voucherModel.create(newVoucher); //property nào ko đc truyền(undefined) thì tự bỏ qua
    res.status(201).json({
      message: "Tao moi voucher thanh cong",
      data: result,
    });
  } catch (error) {
    console.log(error);
    if (error.code === 11000) {
      return res.status(400).json({
        message: "Ma voucher da ton tai trong he thong",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//update by id
const updateVoucherById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let voucherId = req.params.voucherId;
    let { maVoucher, phanTramGiamGia, ghiChu } = req.body;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
      return res.status(400).json({
        message: "Id khong hop le",
      });
    }
    if (!maVoucher && maVoucher !== undefined) {
      //undefined tức là ko truyền vào thì thôi khỏi update, ko cần validate
      return res.status(400).json({
        message: "Ma voucher khong hop le",
      });
    }
    if (
      (!Number.isInteger(phanTramGiamGia) || phanTramGiamGia < 0) &&
      phanTramGiamGia !== undefined
    ) {
      //1 số nguyên > 0. Kiểm tra số thực thì dùng Number.isFinite() (isFinite là kiểm tra 1 số hữu hạn)
      //ngoài ra có thể dùng Number.isSafeInteger để kiểm tra số nguyên an toàn(nằm trong khoảng giá trị của type Number)
      return res.status(400).json({
        message: "Ma voucher khong hop le",
      });
    }
    //B3: Xử lý dữ liệu
    let updateVoucher = {
      maVoucher,
      phanTramGiamGia,
      ghiChu,
    };
    let result = await voucherModel.findByIdAndUpdate(voucherId, updateVoucher); //property nào ko đc truyền(undefined) thì tự bỏ qua
    res.status(200).json({
      message: "Cap nhat voucher thanh cong",
      data: result,
    });
  } catch (error) {
    console.log(error);
    if (error.code === 11000) {
      return res.status(400).json({
        message: "Ma voucher da ton tai trong he thong",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//delete by id
const deleteVoucherById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let voucherId = req.params.voucherId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
      return res.status(400).json({
        message: "Id khong hop le",
      });
    }
    //B3: Xử lý dữ liệu
    let result = await voucherModel.findByIdAndDelete(voucherId);
    if (!result) {
      return res.status(404).json({
        message: "Khong tim thay voucher",
      });
    }
    res.status(200).json({
      message: "Xoa voucher thanh cong",
      data: result,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

module.exports = {
  getAllVouchers,
  getVoucherById,
  createVoucher,
  updateVoucherById,
  deleteVoucherById,
};
