const mongoose = require("mongoose");
const orderModel = require("../models/order.model");
const userModel = require("../models/user.model");
const voucherModel = require("../models/voucher.model");
const drinkModel = require("../models/drink.model");

const createOrder = async (req, res) => {
  try {
    //B1: Collect
    const reqOrder = req.body;
    //B2: Validate
    if (
      !reqOrder.kichCo ||
      !reqOrder.loaiPizza ||
      !reqOrder.idLoaiNuocUong ||
      !reqOrder.hoTen ||
      !reqOrder.email ||
      !reqOrder.soDienThoai ||
      !reqOrder.diaChi
    ) {
      return res.status(400).json({
        message: "Required fields are missing",
      });
    }
    if (
      !mongoose.Types.ObjectId.isValid(reqOrder.idVourcher) &&
      reqOrder.idVourcher
    ) {
      return res.status(400).json({
        message: "Invalid voucher ID",
      });
    }
    //B3: Handle
    //tìm user
    let email = reqOrder.email;
    let foundUser = await userModel.findOne({ email });
    if (!foundUser) {
      //nếu chưa tồn tại thì tạo mới user
      let newUser = {
        fullName: reqOrder.hoTen,
        email: reqOrder.email,
        address: reqOrder.diaChi,
        phone: reqOrder.soDienThoai,
        orders: [],
      };
      foundUser = await userModel.create(newUser);
    }
    //check nước uống(có thể bỏ qua vì ở FE đã check r)
    let foundDrink = await drinkModel.findOne({
      maNuocUong: reqOrder.idLoaiNuocUong,
    });
    if (!foundDrink) {
      return res.status(404).json({
        message: "drink not found",
      });
    }
    //check voucher
    let foundVoucher;
    if (reqOrder.idVourcher) {
      foundVoucher = await voucherModel.findById(reqOrder.idVourcher);
      if (!foundVoucher) {
        return res.status(404).json({
          message: "voucher not found",
        });
      }
    }
    //tạo mới order
    let newOrder = {
      pizzaSize: reqOrder.kichCo,
      pizzaType: reqOrder.loaiPizza,
      voucher: foundVoucher ? foundVoucher._id : null,
      drink: foundDrink._id,
    }; //tạo đối tượng ko có userId(quản lý đơn hàng theo người dùng)
    let createdOrder = await orderModel.create(newOrder);

    // Thêm id của đơn hàng mới vào mảng orders của người dùng
    let updatedUser = await userModel
      .findByIdAndUpdate(
        foundUser._id,
        {
          $push: { orders: createdOrder._id },
        },
        { new: true }
      )
      .populate("orders");

    //Tạo đối tượng response
    const resOrder = {
      id: createdOrder._id,
      orderCode: createdOrder.orderCode,
      kichCo: createdOrder.pizzaSize,
      duongKinh: reqOrder.duongKinh,
      suon: reqOrder.suon,
      salad: reqOrder.salad,
      loaiPizza: createdOrder.pizzaType,
      idVourcher: foundVoucher ? foundVoucher._id : null,
      thanhTien: reqOrder.thanhTien,
      giamGia: foundVoucher
        ? (reqOrder.thanhTien * foundVoucher.phanTramGiamGia) / 100
        : 0,
      idLoaiNuocUong: foundDrink._id,
      soLuongNuoc: reqOrder.soLuongNuoc,
      hoTen: updatedUser.fullName,
      email: updatedUser.email,
      soDienThoai: updatedUser.phone,
      diaChi: updatedUser.address,
      loiNhan: reqOrder.loiNhan,
      trangThai: createdOrder.status,
      ngayTao: createdOrder.createdAt,
      ngayCapNhat: createdOrder.updatedAt,
    };
    //trả về
    res.status(201).json(resOrder);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//Hàm get drink list
const getDrinksList = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    //B2: Validate dữ liệu
    //B3: Xử lý dữ liệu
    let drinksList = await drinkModel.find(); //tìm toàn bộ
    res.status(200).json(drinksList);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

module.exports = {
  createOrder,
  getDrinksList,
};
