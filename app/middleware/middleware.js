const logAPIMiddleWare = (req, res, next) => {
    if (!req.method || !req.originalUrl) {//ktra xem có method và url gốc trong request hay ko
        return res.status(400).send("Yêu cầu không hợp lệ!");
    }
    console.log(`[${new Date().toLocaleDateString()}] ${req.method} ${req.originalUrl}`);//lấy thời gian(theo giờ địa phương của server), phương thức và url gốc mà client đã gửi
    next();//chạy tiếp
}

module.exports = logAPIMiddleWare;