const mongoose = require("mongoose"); // Import thư viện

const Schema = mongoose.Schema; // Khởi tạo Schema

// Khai báo đối tượng voucher Schema
const voucherSchema = new Schema(
  {
    maVoucher: {
      type: String,
      unique: true,
      required: true,
    },
    phanTramGiamGia: {
      type: Number,
      required: true,
    },
    ghiChu: {
      type: String,
      required: false,
    },
  },
  {
    timestamps: true, // Áp dụng timestamps cho các trường createdAt và updatedAt
  }
);

// Compile schema thành model và xuất ra ngoài
module.exports = mongoose.model("Voucher", voucherSchema);
