const mongoose = require("mongoose"); //import thư viện

const Schema = mongoose.Schema; //khởi tạo Schema

const userSchema = new Schema(
  {
    //khai báo đối tượng
    // _id: {
    //   type: mongoose.Types.ObjectId, //kiểu dữ liệu của thuộc tính tương ứng vs kiểu dữ liệu mặc định _id của mongoDB
    //   unique: true, //duy nhất
    // },
    fullName: {
      type: String,
      required: true, //bắt buộc có
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    address: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
      unique: true,
    },
    orders: [
      //thuộc tính orders gồm nhiều order nên ở dạng mảng
      {
        type: mongoose.Types.ObjectId,
        ref: "order", //tham chiếu đến order collection
      },
    ],
  },
  {
    timestamps: true,
  }
);

//compile object to model and exports
module.exports = mongoose.model("user", userSchema);
