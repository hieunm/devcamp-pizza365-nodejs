//import thư viện
const mongoose = require("mongoose");
const { v4: uuidv4 } = require("uuid");

const Schema = mongoose.Schema; //khởi tạo Schema

const orderSchema = new Schema(
  {
    //khai báo đối tượng
    // _id: {
    //   type: mongoose.Types.ObjectId,
    //   unique: true,
    // },
    orderCode: {
      type: String,
      unique: true,
      default: function () {
        return uuidv4(); //sử dụng thư viện uuid để tạo orderCode ko trùng(random trong js có tỷ lệ bị lặp(rất nhỏ))
      },
    },
    pizzaSize: {
      type: String,
      required: true,
    },
    pizzaType: {
      type: String,
      required: true,
    },
    voucher: {
      //chỉ đc 1 voucher duy nhất
      type: mongoose.Types.ObjectId, //tham chiếu tới voucher và truy vấn bằng _id nên phải cùng kiểu dữ liệu
      ref: "voucher", //tham chiếu đến collection voucher
    },
    drink: {
      //chỉ đc 1 drink duy nhất
      type: mongoose.Types.ObjectId,
      ref: "drink",
    },
    status: {
      type: String,
      default: "open",
    },
  },
  {
    timestamps: true,
  }
);

//compile object to model and exports
module.exports = mongoose.model("order", orderSchema);
