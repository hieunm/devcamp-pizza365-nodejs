const mongoose = require("mongoose");//import mongoose

const Schema = mongoose.Schema;//Khởi tạo Schema

const drinkSchema = new Schema({//khai báo đối tượng drink schema
  // _id: {//trừ khi cần render id thì khai báo còn nếu để mặc định như này thì ko cần, tránh warning thừa thãi
  //   type: mongoose.Types.ObjectId,//khai báo id có kiểu dữ liệu dạng mongoose.Types.objectId, objectId là kiểu định danh duy nhất để xác định tài liệu, tương ứng với kiểu mặc định DB sinh ra nếu ko setup _id
  //   unique: true,//thuộc tính này là duy nhất. Nếu trùng nhau thì mongoose sẽ tự check và báo lỗi. (error.code === 11000)
  // },
  maNuocUong: {
    type: String,
    unique: true,//thuộc tính này là duy nhất
    required: true,//thuộc tính này phải có đối với mỗi object(ko phải optional)
  },
  tenNuocUong: {
    type: String,
    required: true,
  },
  donGia: {
    type: Number,
    required: true,
  },
}, {
    timestamps: true,//Đây là một tùy chọn để tự động thêm hai trường createdAt và updatedAt vào mỗi bản ghi.
});

//compile schema to model and exports
module.exports = mongoose.model("drink", drinkSchema);//tên model là drink
