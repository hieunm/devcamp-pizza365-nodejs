const express = require('express');
const {
    createOrder,
    getDrinksList
} = require('../controllers/devcamp-pizza365.controller');

const devcampPizza365Router = express.Router();

devcampPizza365Router.post('/orders', createOrder);

devcampPizza365Router.get('/drinks', getDrinksList);

module.exports = devcampPizza365Router;