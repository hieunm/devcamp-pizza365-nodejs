const express = require("express");//import thư viện

const logAPIMiddleWare = require("../middleware/middleware");//import middleware

//import controller
const {
    createOrder,
    getAllOrders,
    getOrderById,
    updateOrderById,
    deleteOrderById
} = require('../controllers/order.controller');

const orderRouter = express.Router();//tạo đối tượng router mới
orderRouter.use(logAPIMiddleWare);//áp dụng function middleware cho toàn bộ router

//Routes CRUD
//create order 
orderRouter.post("/", createOrder);

//get all
orderRouter.get("/", getAllOrders);

//get order by id
orderRouter.get("/:orderId", getOrderById);

//update order 
orderRouter.put("/:orderId", updateOrderById);

//dalete order 
orderRouter.delete("/:orderId/users/:userId", deleteOrderById);

module.exports = orderRouter;