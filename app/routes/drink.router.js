const express = require("express");//import thư viện
const logAPIMiddleWare = require("../middleware/middleware");//import middleware
const {
    getAllDrinks,
    getDrinkById,
    createDrink,
    updateDrinkById,
    deleteDrinkById
    } = require('./../controllers/drink.controller');//import controller


const drinkRouter = express.Router();//tạo đối tượng router mới
drinkRouter.use(logAPIMiddleWare);//áp dụng function middleware cho toàn bộ router

//Routes CRUD
//get all
drinkRouter.get("/", getAllDrinks);

//get drink by id
drinkRouter.get("/:drinkId", getDrinkById);

//create drink 
drinkRouter.post("/", createDrink);

//update drink 
drinkRouter.put("/:drinkId", updateDrinkById);

//dalete drink 
drinkRouter.delete("/:drinkId", deleteDrinkById);

module.exports = drinkRouter;