const drinkModel = require("../models/drink.model");
const voucherModel = require("../models/voucher.model");

const drinks = require("./CRUD_Pizza365.drinks.json");
const vouchers = require("./CRUD_Pizza365.vouchers.json");

const initializeDrinks = async () => {
  try {
    const countDrink = await drinkModel.countDocuments();

    if (countDrink === 0) {
      for (const drink of drinks) {
        await drinkModel.create(drink);
      }
    }
  } catch (error) {
    console.error(error);
  }
};

const initializeVouchers = async () => {
  try {
    const countVoucher = await voucherModel.countDocuments();

    if (countVoucher === 0) {
      for (const voucher of vouchers) {
        await voucherModel.create(voucher);
      }
    }
  } catch (error) {
    console.error(error);
  }
};

const initializeData = async () => {
  await initializeDrinks();
  await initializeVouchers();
};

module.exports = initializeData;
